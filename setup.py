from setuptools import find_packages, setup

setup(
    name="gitlab_egg",
    version="1.6.0",
    description="Lib for services",
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        "sentry_sdk==1.38.0"
    ],
    extras_require={
        "dev": [
            "mypy==1.7.1",
            "flake8==6.1.0",
            "isort==5.13.0",
        ]
    }
)
